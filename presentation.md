---
title: Vendre et se faire acheter
subtitle: Séminaire Multi du 15 septembre
author: thomas.brosset@multi.coop
date: 15 septembre 2023
title-slide-attributes:
  data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 12%"
  data-background-position: "95% 95%, 70% 97%"
---

# Programme de la journée

- 10h: Echange Avant-vente
- 11h30: Atelier ?
- 13h : déjeuner

# J'ai un problème avec le commercial

:::::: {.columns}

::: {.column width="50%"}

![](./images/cameracafe-suite-2021-compressed.jpg)

:::

::: {.column width="50%"}

#forceur

#bullshit

:::

::::::

# L’avant vente comme le support font partie du produit

- Approche design

- Comprendre l’environnement et les motivations

- Éliminer les frictions

# Objectifs de la présentation

- Echanger probablement des banalités sur le travail commercial
- Quelques idées pour transformer l'avant vente en un moment intéressant
  - pour multi
  - pour le client

# L'importance de l'avant vente

## Ce qu'on vend et ce qu'on achète

<img src="./images/patagonia.png" width="200px"/>
<img src="./images/indiehoster.png" width="200px"/>
<img src="./images/office.png" width="200px"/>

### Dans le numérique, on achète très rarement qu'une solution technique

D'ailleurs, la plupart de celles que nous proposons sont gratuites.

## Que veut le client ?

Quand le client nous consulte :

- il a une idée de ce que l'on propose, qui n'est pas forcément la réalité
- il a une idée de ce qu'il veut, basée très souvent sur des hypothèses non testées et une connaissance limitée des solutions possibles
- son besoin n'inclut pas forcément le besoin de son organisation (**_#SSO_**)

## Tromper ou aider ?

Le commercial est-il un forceur ?

**on résoud vos problèmes** vs. **on vous prend tout votre argent**

empathie:

- Comprendre son propre besoin est difficile
- Prendre des décisions est difficile
- Evaluer une solution est difficile

## L'intérêt est très limité de vendre quelque chose que le client ne veut pas

- Il ne donnera pas les bons retours (difficulté de pilotage)
- Il aura forcément des besoins qui ne seront pas satisfaits
- mauvaise perception de la valeur, quelque soit le travail réalisé

**Il y aura des moments désagréables**

# Les pistes pour l'avant vente

## Trois personnas autour de la commande

- l'utilisateur: c'est le premier auquel on pense, mais il est rarement autour de la table
- l'acheteur: celui qui va faire le choix de la solution, qui va être en charge de la négociation
- le décisionnaire: c'est celui qui va valider en dernier lieu la décision d'achat finale

Chaque personnas a une attente/lecture différente de l'offre

## Les principaux biais

1. Biais de surconfiance : Certains utilisateurs peuvent surestimer leurs propres compétences et leurs connaissances, ce qui peut les amener à prendre des décisions d'achat sans approfondir leurs besoins ou les solutions proposées.

## Les principaux biais

2. Biais d'aversion au risque et au changement : Les utilisateurs ont tendance à accorder plus d'importance à éviter les pertes qu'à réaliser des gains équivalents. Cela peut les rendre réticents à prendre des risques, même si une offre présente des avantages substantiels. Cela peut les amener à préférer le status quo.

## Les principaux biais

3. Biais de confirmation : Les utilisateurs ont tendance à rechercher et à privilégier les informations qui confirment leurs croyances ou leurs préférences préexistantes. Cela peut les amener à ignorer des informations importantes ou à sous-estimer les avantages d'une offre concurrente.

## Les principaux biais

4. Biais de confirmation sociale : Les utilisateurs peuvent être influencés par les choix et les opinions de leur entourage, ce qui peut les pousser à suivre la tendance ou à faire des choix conformes aux normes sociales, même si cela n'est pas nécessairement dans leur meilleur intérêt.

## Les principaux biais

5. Biais de disponibilité : Les utilisateurs ont tendance à accorder plus d'importance aux informations qui leur sont facilement accessibles ou qui leur viennent à l'esprit rapidement. Cela peut les amener à surestimer l'importance d'informations récemment entendues ou vues.

## Les principaux biais

6. Biais d'ancrage : Lorsque les utilisateurs sont exposés à un premier chiffre ou à une première information, ils ont tendance à s'y "ancrer" et à l'utiliser comme point de référence pour évaluer d'autres informations. Cela peut entraîner une évaluation inexacte des offres.

## Le cas particulier des Appels d'Offre

Un AO nécessite la rédaction d'un cahier des charges. Il faut pour cela avoir bien compris le problème et avoir une idée précise de la manière dont on veut le résoudre. C'est très rarement le cas.
Pourtant, les acheteurs sont obligés de se soumettre à l'exercice.
Dans nos réponses, il faudra forcément tordre la réponse pour trouver l'équilibre entre:

- le cadre contraint de la réponse
- ce que l'on peut/veut faire
- Tout en étant compris par le client

# L'avant vente comme une prestation ?

## Voir plus loin/large que la prestation

Une prestation n'est jamais isolée. Elle s'inscrit dans l'ensemble de l'organisation.
Il y aura forcément de nouveaux besoins, au moins de maintenance.

**Aider le client à définir sa roadmap est essentiel. **

Evoquer le sujet en avant vente est une marque de professionalisme.

## Acheter prend du temps

Le client a besoin :

- de comprendre ses problèmes
- de planifier (temporalité + budget) ses projets
- d'inscrire le projet dans l'organisation (dsi, accompagnement du changement)
- de définir le/les périmètre(s) et éventuellement de mobiliser les parties concernées

C'est son job, mais s'il ne le fait pas / pas bien, on ne travaille pas

## Eviter les ruptures de charge sur les misisons existantes

Ne pas attendre la fin de la mission pour proposer une suite

- Livrer sans avoir discuté de la suite est un manque de professionalisme
- risque de faire échouer le projet du client (la suite ce n'est pas forcément multi)

A mon sens, cela peut/doit être inclus dans la prestation du PO

# Donner de la force aux présentations commerciales

## Pitcher pour être pitché

:::::: {.columns}

::: {.column width="50%"}

![](./images/elevator_pitch.jpeg)

:::

::: {.column width="50%"}

Les clients décident rarement seuls. Ils vont probablement devoir en parler à leur N+1 et les convaincre.

**Facilitons leur le travail!**

:::

::::::

## La finalité avant la fonctionnalité

:::::: {.columns}

::: {.column width="40%"}

<small>
*1000h de musique dans la poche*

vs.

_baladeur mp3_

vs.

_lecteur de fichiers mp3 et wave avec 32go de mémoire_
</small>
<img src="./images/ipod.png" width="150px"/>

:::

::: {.column width="10%"}
:::
::: {.column width="50%"}

<small>
*Assurer le recrutement d'experts sans conflits d'intérêts*

vs.

_Vérifier les liens d'intérêts des experts_

vs.

_Application web de recherche de données sur les versements des laboratoires pharmaceutiques aux professionels de santé_

</small>

<img src="./images/adex.png" width="150px"/>

:::

::::::

La finalité est rarement exprimée par le client.

## Faire répondre l'offre à la demande

Les caractéristiques de notre offre ne sont pertinentes et ne peuvent être comprises par l’acheteur uniquement si elles répondent à un problème et avec lequel il est d’accord.

**la caractéristique doit toujours être précédé de l’énonciation du problème.**

## voir l'offre dans sa globalité

Cela vaut pour toutes les caractéristiques de l’offre : solution technique, mais aussi équipe, calendrier, prix (?)

## retirer le superflu

Si une caractéristique de l'offre ne répond pas à un problème explicitement posé, alors il faut le retirer, au risque d'apporter de la confusion.

"Nous utiliserons Typescript version 4.1" sera pertinent dans de très rares cas.

## L'importance du cadre et le "cerveau reptilien"

On prend nos décisions avant d'en avoir conscience (post-rationalité)
Ces décisions se font par des mécanismes de comparaison, et non d'évaluation "en valeur absolue". Ainsi, définir à quoi on est comparé est la clé de la persuasion.

## Le Storytelling

:::::: {.columns}

::: {.column width="20%"}
<img src="./images/arthur.png" />

:::

::: {.column width="80%"}

On fonctionne tous de la même façon, on connait tous les mêmes histoires. Se caler sur une histoire que l'interlocuteur connait déjà, c'est lui faciliter la lecture.

**héros -> problème -> aide -> résolution**

C'est le format classique de nos réponses

**contexte -> problématique -> équipe -> solution**

mais on ne le traite pas forcément comme tel
:::
::::::

## Capter l'attention

<img src="./images/logos.jpeg" width="300px" />

Introduire un élément inattendu, choquant ou suprenant permet de capter l'attention.

- Est-ce que je rate une information importante ?
- Quelque chose que je n'ai pas compris sur mon métier ?
- Une idée que je peux ressortir pour garder mon poste / avoir une promotion ?

## Capter l'attention

:::::: {.columns}

::: {.column width="20%"}

<img src="./images/hache.jpg" width="200px" />

:::

::: {.column width="80%"}
Avoir des positions tranchées est à mon sens toujours positif, surtout si c'est bien amené. En effet, soit on touche juste et dans ce cas l'interlocuteur est séduit, soit on est à coté, et dans ce cas, l'interlocuteur peut avoir envie de nous répondre et nous dire pourquoi. Si à ce moment on a la bonne qualité d'écoute, alors on crée un vrai échange avec le client avec la possibilité de sortir du sens et des éléments intéressants. On en sort donc enrichi, et l'interlocuteur peut se sentir valorisé de cet échange.
:::
::::::

## Définir les cadres de référence

<iframe width="560" height="315" src="https://www.youtube.com/embed/9LlWSDol59w?si=gjthMKtVWvr0Qw0q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Il faut choisir la manière dont on veut être perçu.
On sera mis dans une case. Le jeu est de définir quelles seront les cases disponibles.

## Définir les cadres de référence

- Une compétence inédite
- une urgence
- une compréhension particulière du problème

<iframe width="560" height="315" src="https://www.youtube.com/embed/yslq7hxJis8?si=Mi7JFCZzQcJ-GgfT&amp;start=21" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

trouver un angle pertinent qui ne sera pas celui de la concurrence

## La répétition

- ancrer
- plusieurs versions d'une même idée
- répéter et faire répéter

# Les tricks / dark side

## Quand faire de l'avant vente

Toujours.
On n'est pas là pour vendre, on est là pour aider 😎
Qui n'a pas besoin d'aide ?

## Les validations intermédiaires

Rythmer la présentation commerciale avec des moments où l'on permet à l'interlocuteur de valider par un oui permet de le préparer au "oui" final.

Il est plus difficile de dire 1 fois "non" après avoir dit 20 fois "oui" (#forceur).

## ABC of Sales

<img src="./images/abc_closing.jpeg" width="500" />

**Always be closing**

Tant qu'il n'y a pas de réponse, ce n'est pas non. (_#forceur_)

C'est souvent de l'indécision, de la peur.
Si on est sûr de son offre, alors il faut relancer jusqu'au "non", et voir si l'on peut répondre à l'objection.

## La négociation

- Le prix n'est qu'une partie de ce qui peut être négocié
  - contenu de la prestation
  - modalités d'exécution (calendrier, déplacements, personnes)
  - modalités de paiement
  - ...
- Ne jamais couper en deux
- Toujours demander une contre partie

## Préparer la négociation

<img src="./images/Méthode batna.png" width="500" />

Sans préparation, impossible de savoir si 10% de remise met dans le rouge.

# Trucs lus

- Système 1 / Système 2 - Daniel Kahneman
- Le libre arbitre et la science du cerveau - Michael S. Gazzaniga
- Pitch Anything - Oren Klaff
- Comment se faire des amis - Dale Carnegie
- Start with why - Simon Sinek
- Glengary Glen Ross

<iframe width="560" height="315" src="https://www.youtube.com/embed/O6ybfVT9gxA?si=Y2mzeCZ6G6VuH0Vo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
